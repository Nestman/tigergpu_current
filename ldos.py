import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time
from math import pi

from tool import memory_report
from tool import write_results
from tool import initialize
from gce.grid import Grid
from gce import verbose
dtype=np.complex128

def ldos_slab(shape,j,x1,b,epsBkg,epsdiff,solver,A,hxyz,omega):
    def ldos_nlopt(dof,grad):
        # bg is preconditioned
        if comm.rank==0:
            # assemble epsilon
            doftmp = dof.reshape(A.Mx,A.My)
            ep = dtype(np.copy(epsBkg))
            A.matA(doftmp,ep,epsdiff)
            e=[ep,ep,ep]
            initialize(x1, verbose.count, verbose.init, verbose.init_type, shape, dtype)
        else:
            e=[None]*3
            
        # update solvers
        solver.update_ep(e)

        xg = [Grid(dtype(f), x_overlap=1) for f in x1]
        xg, err, success = solver.Multisolve(b, xg)
        
        field=[E.get() for E in xg]
        x1[:]=field

        if comm.rank==0:
            field=solver.post_cond(field)
            #ldos = -Re[J* E]
            val=0
            for i in range(3):
                val += np.vdot(field[i],j[i])*hxyz
            val=-np.real(val)
        else:
            val=0
        val = comm.bcast(val)

        if comm.rank==0:
            print "At freq = ",np.real(omega)/2/pi," and count ",verbose.count,", ldos = ",val

            if verbose.export_in_object is 1 and verbose.count%verbose.outputbase is 0:
                np.savetxt(verbose.filename+'dof'+str(verbose.count)+'.txt', dof)
                
            if verbose.outputfield == 1:
                write_results('x.h5',field)
                
        if verbose.export_in_object is 1:
            verbose.count +=1

        # gradient information: Re[ (omega_complex/i) E E ]
        if grad.size>0 and verbose.outputfield==0:
            if comm.rank==0:
                E2=[np.real(field[i]*field[i] *omega/1j) for i in range(3)]
                
                gradtmp=np.zeros((A.Mx,A.My))
                A.matAT(gradtmp,E2)
                grad[:]=gradtmp.flatten()*hxyz*epsdiff

            grad[:]=comm.bcast(grad)
            
        memory_report(str(verbose.count)+'-')
        return val
    return ldos_nlopt


def coupler_total(shape, b1, x1, x2, u1, u2, solver1, solver2, epsBkg1 ,epsBkg2, epsdiff1, epsdiff2, A, omega1, omega2, overlap, polarization1, polarization2):
    def coupler_nlopt(dof,grad):
        # b1 is already preconditioned
        if comm.rank==0:
            # assemble epsilon
            doftmp = dof.reshape(A.Mx,A.My)
            # epsilon at w1
            ep1 = dtype(np.copy(epsBkg1))
            A.matA(doftmp,ep1,epsdiff1)
            e1=[ep1,ep1,ep1]
            # epsilon at w2
            ep2 = dtype(np.copy(epsBkg2))
            A.matA(doftmp,ep2,epsdiff2)
            e2=[ep2,ep2,ep2]
            # pure dof
            eps = dtype(np.zeros(shape))
            A.matA(doftmp,eps,1.0)
            
            initialize(x1, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            initialize(x2, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            initialize(u1, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            initialize(u2, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            
        else:
            e1=[None]*3
            e2=[None]*3
            eps=[None]
        # update solvers
        solver1.update_ep(e1)
        solver2.update_ep(e2)
        # solve at w1
        xg1 = [Grid(dtype(f), x_overlap=1) for f in x1]

        xg1, err, success = solver1.Multisolve(b1, xg1)
        
        field1=[E.get() for E in xg1]
        x1[:]=field1

        # setting up J2= eps (e1.x1.x1).e2
        b2=[None]*3
        if comm.rank==0:
            field1=solver1.post_cond(field1)
            if verbose.outputfield == 1:
                write_results('x1.h5',field1)
            
            J2 = field1[polarization1]*field1[polarization1]*eps
            b2[polarization2]= J2 *-1j* omega2
            b2[(polarization2+1)%3]= dtype(np.zeros(shape))
            b2[(polarization2-1)%3]= dtype(np.zeros(shape))
            b2=solver2.pre_cond(b2) #preconditioner
            
        # b2 will be modified 
        xg2 = [Grid(dtype(f), x_overlap=1) for f in x2]
        xg2, err, success = solver2.Multisolve(b2, xg2)

        # computing val=real(x2,lap)
        field2=[E.get() for E in xg2]
        x2[:]=field2
        if comm.rank==0:
            field2=solver2.post_cond(field2)
            val=0
            for i in range(3):
                val += np.vdot(overlap[i],field2[i])
            val = np.real(val)

            if verbose.outputfield == 1:
                write_results('x2.h5',field2)
                
            print "At count ",verbose.count,", val = ",val
            if verbose.export_in_object is 1 and verbose.count%verbose.outputbase is 0:
                np.savetxt(verbose.filename+'dof'+str(verbose.count)+'.txt', dof)
        else:
            val=0
        val = comm.bcast(val)
        if verbose.export_in_object is 1:
            verbose.count +=1
        
        # gradient information
        if grad.size>0 and verbose.outputfield==0:
            #u1 = M2^-1 -i omega2 lap
            bu1=[None]*3
            if comm.rank==0:
                bu1 = [np.copy(f)*-omega2*1j for f in overlap]
                bu1=solver2.pre_cond(bu1) #preconditioner
            u1g = [Grid(dtype(f), x_overlap=1) for f in u1]
            
            u1g, err, success = solver2.Multisolve(bu1, u1g)

            u1f=[E.get() for E in u1g]
            u1[:]=u1f
            if comm.rank==0:
                u1f=solver2.post_cond(u1f)
                
            #u2 = M1^-1 (x1 e1 B e2 eps u1 )
            bu2=[None]*3
            if comm.rank==0:
                ju2 = u1f[polarization2]*eps
                bu2[polarization1]= ju2 * field1[polarization1]
                bu2[(polarization1+1)%3]= dtype(np.zeros(shape))
                bu2[(polarization1-1)%3]= dtype(np.zeros(shape))
                bu2=solver1.pre_cond(bu2) #preconditioner
            u2g = [Grid(dtype(f), x_overlap=1) for f in u2]
            u2g, err, success = solver1.Multisolve(bu2, u2g)            
            
            u2f=[E.get() for E in u2g]
            u2[:]=u2f
            if comm.rank==0:
                u2f=solver1.post_cond(u2f)
                gradtmp=dtype(np.zeros((A.Mx,A.My)))
                #g0 = i omega2 epsDiff2 u1. x2
                g=[u1f[i]*field2[i]*1j*omega2*epsdiff2 for i in range(3)]
                A.matAT(gradtmp,g)
                #g1 = 2w1^2 epsDiff1 x1 u2
                g=[field1[i]*u2f[i]*2*omega1*omega1*epsdiff1 for i in range(3)]
                A.matAT(gradtmp,g)
                #g2 = u1 (e2 B e1 x1 x1)
                gt = field1[polarization1]*field1[polarization1]
                g[polarization2]= gt * u1f[polarization2]
                g[(polarization2+1)%3]= dtype(np.zeros(shape))
                g[(polarization2-1)%3]= dtype(np.zeros(shape))
                A.matAT(gradtmp,g)
                
                grad[:]=np.real(gradtmp.flatten())

            grad[:]=comm.bcast(grad)
            
        memory_report(str(verbose.count)+'-')
        return val
    return coupler_nlopt

def coupler_normalize(shape, b1, x1, x2, u1, u2, u3, solver1, solver2, epsBkg1 ,epsBkg2, epsdiff1, epsdiff2, A, overlap, f1abs, polarization1, polarization2, powerindex):
    def coupler_nlopt(dof,grad):
        # b1 is already preconditioned
        if comm.rank==0:
            # assemble epsilon
            doftmp = dof.reshape(A.Mx,A.My)
            # epsilon at w1
            ep1 = dtype(np.copy(epsBkg1))
            A.matA(doftmp,ep1,epsdiff1)
            e1=[ep1,ep1,ep1]
            # epsilon at w2
            ep2 = dtype(np.copy(epsBkg2))
            A.matA(doftmp,ep2,epsdiff2)
            e2=[ep2,ep2,ep2]
            # pure dof
            eps = dtype(np.zeros(shape))
            A.matA(doftmp,eps,1.0)
            
            initialize(x1, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            initialize(x2, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            initialize(u1, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            initialize(u2, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            initialize(u3, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            
        else:
            e1=[None]*3
            e2=[None]*3
            eps=[None]
        # update solvers
        solver1.update_ep(e1)
        solver2.update_ep(e2)
        # solve at w1
        xg1 = [Grid(dtype(f), x_overlap=1) for f in x1]
        xg1, err, success = solver1.Multisolve(b1, xg1)
        
        field1=[E.get() for E in xg1]
        x1[:]=field1
        
        # compute l1= f1abs*|x1|^2, setting up J2= eps (e1.x1.x1).e2
        b2=[None]*3
        l1=0.0
        if comm.rank==0:
            field1=solver1.post_cond(field1)
            for i in range(3):
                l1 += np.sum(np.conj(field1[i])*field1[i]*f1abs[i])
            l1 = np.real(l1)
            print "At count ",verbose.count,", l1 = ",l1
            
            if verbose.outputfield == 1:
                write_results('x1.h5',field1)
            
            J2 = field1[polarization1]*field1[polarization1]*eps
            b2[polarization2]= J2 *-1j* solver2.omega
            b2[(polarization2+1)%3]= dtype(np.zeros(shape))
            b2[(polarization2-1)%3]= dtype(np.zeros(shape))
            b2=solver2.pre_cond(b2) #preconditioner
            
        l1 = comm.bcast(l1)
        # b2 will be modified 
        xg2 = [Grid(dtype(f), x_overlap=1) for f in x2]
        xg2, err, success = solver2.Multisolve(b2, xg2)

        # computing l2=real(x2,lap)
        field2=[E.get() for E in xg2]
        x2[:]=field2
        l2=0.0
        fom=0.0
        if comm.rank==0:
            field2=solver2.post_cond(field2)
            for i in range(3):
                l2 += np.vdot(overlap[i],field2[i])
            l2 = np.real(l2)

            if verbose.outputfield == 1:
                write_results('x2.h5',field2)
            print "At count ",verbose.count,", l2 = ",l2
            
            fom = l2/(l1**powerindex)
            print "At count ",verbose.count,", fom = ",fom
            if verbose.export_in_object is 1 and verbose.count%verbose.outputbase is 0:
                np.savetxt(verbose.filename+'dof'+str(verbose.count)+'.txt', dof)
        l2 = comm.bcast(l2)
        fom = comm.bcast(fom)
        if verbose.export_in_object is 1:
            verbose.count +=1
        
        # gradient information
        # fom'=l2'/l1^p-p*l2*l1'/l1^(p+1)
        if grad.size>0 and verbose.outputfield==0:
            #u3 = M1^-1 f1abs*conj(x1)
            bu3=[None]*3
            if comm.rank==0:
                bu3 = [f1abs[f]*np.conj(field1[f]) for f in range(3)]
                bu3=solver1.pre_cond(bu3) #preconditioner
            u3g = [Grid(dtype(f), x_overlap=1) for f in u3]
            u3g, err, success = solver1.Multisolve(bu3, u3g)

            u3f=[E.get() for E in u3g]
            u3[:]=u3f
            if comm.rank==0:
                u3f=solver1.post_cond(u3f)
                
            #u1 = M2^-1 -i omega2 lap
            bu1=[None]*3
            if comm.rank==0:
                bu1 = [np.copy(f)*-solver2.omega*1j for f in overlap]
                bu1=solver2.pre_cond(bu1) #preconditioner
            u1g = [Grid(dtype(f), x_overlap=1) for f in u1]
            u1g, err, success = solver2.Multisolve(bu1, u1g)

            u1f=[E.get() for E in u1g]
            u1[:]=u1f
            if comm.rank==0:
                u1f=solver2.post_cond(u1f)
                
            #u2 = M1^-1 (x1 e1 B e2 eps u1 )
            bu2=[None]*3
            if comm.rank==0:
                ju2 = u1f[polarization2]*eps
                bu2[polarization1]= ju2 * field1[polarization1]
                bu2[(polarization1+1)%3]= dtype(np.zeros(shape))
                bu2[(polarization1-1)%3]= dtype(np.zeros(shape))
                bu2=solver1.pre_cond(bu2) #preconditioner
            u2g = [Grid(dtype(f), x_overlap=1) for f in u2]
            u2g, err, success = solver1.Multisolve(bu2, u2g)            
            
            u2f=[E.get() for E in u2g]
            u2[:]=u2f
            if comm.rank==0:
                u2f=solver1.post_cond(u2f)
                gradtmp=dtype(np.zeros((A.Mx,A.My)))
                #g0 = i omega2 epsDiff2 u1. x2
                g=[u1f[i]*field2[i]*1j*solver2.omega*epsdiff2 for i in range(3)]
                A.matAT(gradtmp,g)
                #g1 = 2w1^2 epsDiff1 x1 u2
                g=[field1[i]*u2f[i]*2*solver1.omega**2*epsdiff1 for i in range(3)]
                A.matAT(gradtmp,g)
                #g2 = u1 (e2 B e1 x1 x1)
                gt = field1[polarization1]*field1[polarization1]
                g[polarization2]= gt * u1f[polarization2]
                g[(polarization2+1)%3]= dtype(np.zeros(shape))
                g[(polarization2-1)%3]= dtype(np.zeros(shape))
                A.matAT(gradtmp,g)

                grad1=dtype(np.zeros((A.Mx,A.My)))
                #l1' = 2Re(w1^2*epsdiff1*x1*u3)
                g=[2*u3f[i]*field1[i]*solver1.omega**2*epsdiff1 for i in range(3)]
                A.matAT(grad1,g)                
                
                grad[:]=np.real(gradtmp.flatten()/(l1**powerindex)-grad1.flatten()*powerindex*l2/(l1**(powerindex+1)))

            grad[:]=comm.bcast(grad)
            
        memory_report(str(verbose.count)+'-')
        return fom
    return coupler_nlopt

def shg_slab(shape, j1, b1, x1, x2, u1, u2, u3, solver1, solver2, epsBkg1 ,epsBkg2, epsdiff1, epsdiff2, A, omega1, omega2, polarization1, polarization2,powerindex,hxyz):
    def shg_nlopt(dof,grad):
        # b1 is already preconditioned
        if comm.rank==0:
            # assemble epsilon
            doftmp = dof.reshape(A.Mx,A.My)
            # epsilon at w1
            ep1 = dtype(np.copy(epsBkg1))
            A.matA(doftmp,ep1,epsdiff1)
            e1=[ep1,ep1,ep1]
            # epsilon at w2
            ep2 = dtype(np.copy(epsBkg2))
            A.matA(doftmp,ep2,epsdiff2)
            e2=[ep2,ep2,ep2]
            # pure dof
            eps = dtype(np.zeros(shape))
            A.matA(doftmp,eps,1.0)
            
            initialize(x1, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            initialize(x2, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            initialize(u1, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            initialize(u2, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            initialize(u3, verbose.count, verbose.init, verbose.init_type, shape, dtype)
            
        else:
            e1=[None]*3
            e2=[None]*3
            eps=[None]
        # update solvers
        solver1.update_ep(e1)
        solver2.update_ep(e2)
        # solve at w1
        xg1 = [Grid(dtype(f), x_overlap=1) for f in x1]
        xg1, err, success = solver1.Multisolve(b1, xg1)
        
        field1=[E.get() for E in xg1]
        x1[:]=field1
        
        ldos1=0.0;ldos2=0.0
        fom=0.0
        # compute ldos1, setting up J2= eps (e1.x1.x1).e2
        b2=[None]*3
        if comm.rank==0:
            field1=solver1.post_cond(field1)
            # computing ldos1=-real(x1,J1)
            for i in range(3):
                ldos1 += np.vdot(field1[i],j1[i])
            ldos1=-np.real(ldos1)*hxyz
            print "At count ",verbose.count,", ldos1 = ",ldos1
                
            if verbose.outputfield == 1:
                write_results('x1.h5',field1)
            
            J2 = field1[polarization1]*field1[polarization1]*eps
            b2[polarization2]= J2 *-1j* omega2
            b2[(polarization2+1)%3]= dtype(np.zeros(shape))
            b2[(polarization2-1)%3]= dtype(np.zeros(shape))
            b2=solver2.pre_cond(b2) #preconditioner
        ldos1 = comm.bcast(ldos1)
        
        xg2 = [Grid(dtype(f), x_overlap=1) for f in x2]
        xg2, err, success = solver2.Multisolve(b2, xg2)

        # computing fom=-real(x2,J2)
        field2=[E.get() for E in xg2]
        x2[:]=field2
        if comm.rank==0:
            field2=solver2.post_cond(field2)
            ldos2 = np.vdot(field2[polarization2],J2)
            ldos2 = -np.real(ldos2)*hxyz
            print "At count ",verbose.count,", ldos2 = ",ldos2

            if verbose.outputfield == 1:
                write_results('x2.h5',field2)
            # fom = ldos2/ldos1^p
            fom = ldos2/(ldos1**powerindex)
            print "At count ",verbose.count,", fom = ",fom
            if verbose.export_in_object is 1 and verbose.count%verbose.outputbase is 0:
                np.savetxt(verbose.filename+'dof'+str(verbose.count)+'.txt', dof)
                
        fom = comm.bcast(fom)
        ldos2 = comm.bcast(ldos2)
        if verbose.export_in_object is 1:
            verbose.count +=1
        
        # gradient information
        # fom'=ldos2'/ldos1^p-p*ldos2*ldos1'/ldos1^(p+1)
        if grad.size>0 and verbose.outputfield==0:
            #---------------- ldos1' -------------#
            # ldos1'= = -Re[i omega1 ediff1 E1 E1]
            if comm.rank==0:
                g=[np.real(epsdiff1*field1[i]*field1[i] *omega1/1j)*hxyz for i in range(3)]
                grad1=np.zeros((A.Mx,A.My))
                A.matAT(grad1,g)
                grad1[:] *= -powerindex*ldos2/(ldos1**(powerindex+1))
            #---------------- ldos2' -------------#    
            #u1 = M2^-1 -i omega2 conj(J2)
            bu1=[None]*3
            if comm.rank==0:
                bu1[polarization2]= np.conj(J2)*omega2/1j
                bu1[(polarization2+1)%3]= dtype(np.zeros(shape))
                bu1[(polarization2-1)%3]= dtype(np.zeros(shape))
                bu1=solver2.pre_cond(bu1) #preconditioner
            u1g = [Grid(dtype(f), x_overlap=1) for f in u1]
            
            u1g, err, success = solver2.Multisolve(bu1, u1g)

            u1f=[E.get() for E in u1g]
            u1[:]=u1f
            if comm.rank==0:
                u1f=solver2.post_cond(u1f)
                
            #u2 = M1^-1 (x1 e1 B e2 eps conj(x2) )
            bu2=[None]*3
            if comm.rank==0:
                ju2 = np.conj(field2[polarization2])*eps
                bu2[polarization1]= ju2 * field1[polarization1]
                bu2[(polarization1+1)%3]= dtype(np.zeros(shape))
                bu2[(polarization1-1)%3]= dtype(np.zeros(shape))
                bu2=solver1.pre_cond(bu2) #preconditioner
            u2g = [Grid(dtype(f), x_overlap=1) for f in u2]
            u2g, err, success = solver1.Multisolve(bu2, u2g)            
            
            u2f=[E.get() for E in u2g]
            u2[:]=u2f
            if comm.rank==0:
                u2f=solver1.post_cond(u2f)
            #u3 = M1^-1 (x1 e1 B e2 eps u1)
            bu3=[None]*3
            if comm.rank==0:
                ju3 = u1f[polarization2]*eps
                bu3[polarization1]= ju3 * field1[polarization1]
                bu3[(polarization1+1)%3]= dtype(np.zeros(shape))
                bu3[(polarization1-1)%3]= dtype(np.zeros(shape))
                bu3=solver1.pre_cond(bu3) #preconditioner
            u3g = [Grid(dtype(f), x_overlap=1) for f in u3]
            u3g, err, success = solver1.Multisolve(bu3, u3g)            
            
            u3f=[E.get() for E in u3g]
            u3[:]=u3f
            if comm.rank==0:
                u3f=solver1.post_cond(u3f)

                gradtmp=dtype(np.zeros((A.Mx,A.My)))
                # g0=-conj(e2 B e1 x1 x1). x2
                gt = field1[polarization1]*field1[polarization1]
                g[polarization2]= -np.conj(gt) * field2[polarization2]
                g[(polarization2+1)%3]= dtype(np.zeros(shape))
                g[(polarization2-1)%3]= dtype(np.zeros(shape))
                A.matAT(gradtmp,g)
                #g1=-2conj(w1^2 epsDiff1 x1 u2)
                g=[-2*np.conj(omega1*omega1*epsdiff1*field1[i]*u2f[i]) for i in range(3)]
                A.matAT(gradtmp,g)
                #g2=-i omega2 epsDiff2 u1. x2
                g=[-1j*omega2*epsdiff2*u1f[i]*field2[i] for i in range(3)]
                A.matAT(gradtmp,g)
                #g3=-(e2 B e1 x1 x1) u1
                gt = field1[polarization1]*field1[polarization1]
                g[polarization2]= -gt * u1f[polarization2]
                g[(polarization2+1)%3]= dtype(np.zeros(shape))
                g[(polarization2-1)%3]= dtype(np.zeros(shape))
                A.matAT(gradtmp,g)
                #g4=-2w1^2 epsDiff1 x1 u3
                g=[-2*omega1*omega1*epsdiff1*field1[i]*u3f[i] for i in range(3)]
                A.matAT(gradtmp,g)
                
                grad[:]=np.real(hxyz*gradtmp.flatten()/(ldos1**powerindex)+grad1.flatten())

            grad[:]=comm.bcast(grad)
        memory_report(str(verbose.count)+'-')
        return fom
    return shg_nlopt
