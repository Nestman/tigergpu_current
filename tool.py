import h5py, sys
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import os, psutil

def conditioners(pml_p,pml_d, dtype):
    """
    pre- and post- precoonditioner to symmetrize matrix M
    """
    def reshaper(f):
        for k in range(3):
            new_shape = [1, 1, 1]
            new_shape[k] = f[k].size
            f[k] = f[k].reshape(new_shape)
        return f
    # Consts that are used.
    sqrt_sc_pml_0 = reshaper([dtype(np.sqrt(x)**1) for x in pml_p])
    sqrt_sc_pml_1 = reshaper([dtype(np.sqrt(x)**1) for x in pml_d])
    inv_sqrt_sc_pml_0 = reshaper([dtype(np.sqrt(x)**-1) for x in pml_p])
    inv_sqrt_sc_pml_1 = reshaper([dtype(np.sqrt(x)**-1) for x in pml_d])
    # Define the actual functions.
    def apply_cond(x, t0, t1):
        x[0] *= t1[0] * t0[1] * t0[2]
        x[1] *= t0[0] * t1[1] * t0[2]
        x[2] *= t0[0] * t0[1] * t1[2]
        return x

    def pre_step(x):
        return apply_cond(x, sqrt_sc_pml_0, sqrt_sc_pml_1)

    def post_step(x):
        return apply_cond(x, inv_sqrt_sc_pml_0, inv_sqrt_sc_pml_1)

    return pre_step, post_step

    
def write_results(filename,x):
    f=h5py.File(filename,'w')
    for k in range(3):
        data=np.real(x[k]).astype(np.float32)
        d=f.create_dataset('xyz'[k]+'r',data=data)

        data=np.imag(x[k]).astype(np.float32)
        d=f.create_dataset('xyz'[k]+'i',data=data)
    f.close()

def initialize(x, count, init, init_type, shape, dtype):
    if count<init:
        print "Now at step ", count, " out of ",init," are initialized as ",init_type
        if init_type == 'zero':
            x[:] = list(dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)]))
        elif init_type == 'rand':
            x[:] = list(dtype([np.random.randn(*shape),np.random.randn(*shape),np.random.randn(*shape)]))
            for i in range(3):
                x[i] /= np.linalg.norm(x[i])
        else:
            return 0
    return 0
            
class PML:
    def __init__(self, NPML, omega, N, hx, m=4.0, R=16.0):
        if NPML>0:
            sigma=(m+1)/2/(NPML*hx)*R
            xd=np.linspace(1, N, N)*hx
            xp=xd-0.5*hx #dual, prime here make up the interlacing Yee grid--Pengning

            # prime
            lower=NPML*hx-xp
            upper=xp-(N-NPML)*hx

            l = lower*(lower>0)+upper*(upper>0);
            self.sp=1-1j*(sigma / omega) * (l / NPML/ hx)**m;
        
            # dual
            lower=NPML*hx-xd;
            upper=xd-(N-NPML)*hx;

            l = lower*(lower>0)+upper*(upper>0);
            self.sd=1-1j*(sigma / omega) * (l / NPML/hx)**m;
        else:
            self.sp=np.ones(N)
            self.sd=np.ones(N)
    
class interp:
    def __init__(self,Mx,My,Mz,Mzslab,Nx,Ny,Nz,Mx0,My0,Mz0):
        self.Mx=Mx
        self.My=My
        self.Mz=Mz
        self.Mzslab=Mzslab
        self.Mx0=Mx0
        self.My0=My0
        self.Mz0=Mz0
        self.Nx=Nx
        self.Ny=Ny
        self.Nz=Nz
        
    def matA(self,dof,ep,epsdiff):
        if self.Mzslab is 1:
            for i in range(self.Mz0,self.Mz0+self.Mz):
                ep[self.Mx0:self.Mx0+self.Mx,self.My0:self.My0+self.My,i] += dof*epsdiff
        else:
            ep[self.Mx0:self.Mx0+self.Mx,self.My0:self.My0+self.My,self.My0:self.Mz0+self.Mz] += dof*epsdiff
    def matAT(self,dof,e):
        if self.Mzslab is 1:
            for i in range(self.Mz0,self.Mz0+self.Mz):
                for j in range(3):
                    dof += e[j][self.Mx0:self.Mx0+self.Mx,self.My0:self.My0+self.My,i]
        else:
            for j in range(3):
                dof += ep[j][self.Mx0:self.Mx0+self.Mx,self.My0:self.My0+self.My,self.My0:self.Mz0+self.Mz]


def gradient_check(fun, dof, grad, index=0, Ntotal=10, start=0.0, dx=1e-2):
    doftmp=np.copy(dof)
    for tmp in np.linspace(start,start+(Ntotal-1)*dx,Ntotal):
        doftmp[index]=tmp
        val=fun(doftmp,grad)
        if comm.rank==0:
            print "At point",index," with dof = ",doftmp[index], ", val =",val,", gradient =",grad[index]
    
def memory_report(ST=''):
    process = psutil.Process(os.getpid())
    h=process.memory_info().rss/1024/1024.0
    print ST+ 'At core',str(comm.rank),' Used Memory = ' + str(h) +' MB'
