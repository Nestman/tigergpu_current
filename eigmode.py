import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time
from math import pi
from cmath import sqrt

from tool import memory_report
from tool import write_results
from gce.grid import Grid
from gce import verbose
dtype=np.complex128

def eigcompute(eps,current,solver, N, iteration, x0):
    # assebmle orthonormal basis
    x=[]
    btmp=[None]*3
    for i in range(N):
        if comm.rank == 0:
            btmp=[-1j*solver.omega*current[i][m] for m in range(3)]
            btmp=solver.pre_cond(btmp)
        xg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in x0]
        xg, err, success = solver.Multisolve(btmp, xg)

        xtmp=[E.get() for E in xg]
        if comm.rank==0:
            x.append(Normalize(solver.post_cond(xtmp),eps))
            
        # power iterations
        for j in range(iterations-1):
            if comm.rank == 0:
                btmp = [np.copy(x[i][m])*eps for m in range(3)]
                btmp=solver.pre_cond(btmp)
            xg = [Grid(dtype(f), x_overlap=1) for f in xtmp]
            xg, err, success = solver.Multisolve(btmp, xg)
            xtmp=[E.get() for E in xg]
            if comm.rank==0:
                x[i]=Normalize(solver.post_cond(xtmp),eps)

    # QR factorization
    Q=[]
    MQ=[]
    if comm.rank == 0:
        Q = GetQR(x,eps)
    # smaller matrix
    ## assemble M*x
    for i in range(N):
       if comm.rank==0:
           btmp = [np.copy(Q[i][m]) for m in range(3)]
        bg = [Grid(dtype(btmp), x_overlap=1) for f in btmp]
        xg = [Grid(dtype, x_overlap=1) for m in range(3)]
        solver.alpha_step(1.0, 1.0, bg, solver.zeros(), xg)
           
def Inner(x,y,z=None):
    val=dtype(0.0)
    if z is None:
        z=1.0
    for m in range(3):
        val += np.sum(x[m]*y[m]*z)
    return val

def Normalize(x,eps):
    norm = sqrt(Inner(x,x,eps))
    xn = [x[i]/norm for i in range(3)]
    return xn
    
def GetQR(x,eps):
    # x=[E1,E2,...],Ei=[Ex,Ey,Ez], eps=[eps_xx]
    N=len(x)
    Q=[]
    for i in range(N):
        tmp = [np.copy(f) for f in x[i]]
        Q.append(tmp)
        
        for j in range(i):
            # inner product = Q[i]*Q[j]*eps
            inner =Inner(Q[i],Q[j],eps)
            Q[i] = [Q[i][m]-inner*Q[j][m] for m in range(3)]
            
        # norm = sqrt(Q[i]*Q[i]*eps)
        Q[i] = Normalize(Q[i],eps)
    return Q
