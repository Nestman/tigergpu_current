#!/bin/bash
#SBATCH --job-name=Qminimax
#SBATCH --output=Pyntg_z_1512d5x1512d5x250nm_minimax_halfstart_raddiff_Qabs200.txt
#SBATCH --gres=gpu:1
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=01:00:00
#SBATCH --mem-per-cpu=5000
#SBATCH --error=Pyntg_z_1512d5x1512d5x250nm_minimax_halfstart_raddiff_Qabs200.err
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-user=pengning@princeton.edu

module load anaconda/5.3.0 openmpi/gcc/2.1.0/64 cudatoolkit/9.0 intel/17.0/64/17.0.5.239

prop=Poynting_z_minimaxopt.py
np=1
Job=1 #Job 0 obj. eval, -1 grad check 1 optimization
freq=0.98116 #for Kai Mei's work unit lambda=625nm
Qabs=200
bproj=0.0
R=0
alpha=3.0

Nx=195
Ny=195
Nz=160
Mx=121
My=121
Mz=20 #125nm thick slab
Mzslab=1

hx=0.02 #for Kai Mei's work unit lambda=625nm
hy=0.02
hz=0.02

Npmlx=12 #roughtly 0.2*wvlgth
Npmly=12
Npmlz=35

epsbkg=1
epssub=5.8564 #diamond
epsdiff=9.9561 #GaP, eps=10.9561
epstype='half' #one for slab start, rand for rand start, file for file start, vac for vac start, half for half start
epsfile='dof_Qabs400.txt' #dof init file if epstype='file'

Jamp=1000.0
#Jdir=1 #x:0 y:1 z:2, implement an average of all pols?
cx=$(($Nx/2))
cy=$(($Ny/2))
#cz=$(($Nz/2))
zdepth=9
upper=-1

# solver
maxit=100000
tol=1e-6
verbose=1
init=2
init_type='rand'

# nlopt
maxeval=1000

outputbase=10
solverbase=5000

mpiexec -n $np python $prop -Job $Job -upper $upper -freq $freq -Qabs $Qabs -bproj $bproj -R $R -alpha $alpha -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mzslab $Mzslab -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg $epsbkg -epssub $epssub -epsdiff $epsdiff -epstype $epstype -epsfile $epsfile -Jamp $Jamp -cx $cx -cy $cy -zdepth $zdepth -maxit $maxit -tol $tol -verbose $verbose -outputbase $outputbase -solverbase $solverbase -maxeval $maxeval -init $init -init_type $init_type
