import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
from gce import verbose
from gce.grid import Grid
from operators import alpha_step as A_step
from operators import rho_step as R_step
from tool import conditioners
import time
dtype=np.complex128

class Solver:
    def __init__(self, shape, err_thresh, max_iters, pml_p, pml_d, omega, alpha_step=None):
        self.rho_step = R_step(dtype)
        self.alpha_step = alpha_step
        self.zeros = lambda: [Grid(dtype, x_overlap=1) for k in range(3)]
        self.err_thresh = err_thresh
        self.max_iters = max_iters
        self.shape = shape
        self.pml_p = pml_p
        self.pml_d = pml_d
        self.omega = omega
        # preconditioner
        if comm.rank==0:
            pre_cond, post_cond = conditioners(pml_p,pml_d, dtype)
        else:
            pre_cond = None
            post_cond = None
            
        self.pre_cond=pre_cond
        self.post_cond=post_cond
        
        if alpha_step is None:
            # initialized with vacuum
            if comm.rank == 0:
                ep = [np.ones(shape),np.ones(shape),np.ones(shape)]
            else:
                ep = [None]*3
                
            self.update_ep(ep,muinv=1)
        
    def update_ep(self,ep,muinv=1):
        if muinv == 1:
            if comm.rank == 0:
                muinv = [np.ones(self.shape),np.ones(self.shape),np.ones(self.shape)]
            else:
                muinv = [None]*3
        
        epw2 = [None]*3
        if comm.rank == 0:
            epw2 = [self.omega**2 * f for f in ep]

        attempts = 0
        while attempts<10:
            try:
                self.alpha_step = A_step(self.shape,self.pml_p,self.pml_d,epw2,muinv,dtype)
                break
            except:
                attempts +=1
                print "attempt",attempts
        if attempts>0 and comm.rank == 0:
            print "****tried",attempts," times"

    def bicg(self, r, x=None):
        # Note: r is used instead of b in the input parameters of the function.
        # This is in order to initialize r = b, and to inherently disallow access to 
        # b from within this function.

        # Initialize variables. 
        # Note that r = b was "initialized" in the function declaration.
        
        """ Lumped bi-conjugate gradient solve of a symmetric system.

        Input variables:
        b -- the problem to be solved is A * x = b.

        Keyword variables:
        x -- initial guess of x, default value is 0.
        rho_step -- rho_step(alpha, p, r, v, x) updates r and x, and returns
        rho and the error. Specifically, rho_step performs:
        x = x + alpha * p
        r = r - alpha * v
        rho_(k+1) = (r dot r)
        err = (conj(r) dot r)
        alpha_step -- alpha_step(rho_k, rho_(k-1), p, r, v) updates p and v, and 
        returns alpha. Specifically, alpha_step performs:
        p = r + (rho_k / rho_(k-1)) * p
        v = A * p
        alpha = rho_k / (p dot v)
        zeros -- zeros() creates a zero-initialized vector. 
        err_thresh -- the relative error threshold, default 1e-6.
        max_iters -- maximum number of iterations allowed, default 1000.

        Output variables:
        x -- the approximate answer of A * x = b.
        err -- a numpy array with the error value at every iteration.
        success -- True if convergence was successful, False otherwise.
        """

        # Initialize x = 0, if defined.
        if x is None: # Default value of x is 0.
            x = self.zeros()

        # Initialize v = Ax.
        v = self.zeros()
        self.alpha_step(1, 1, x, self.zeros(), v) # Used to calculate v = Ax.

        p = self.zeros() # Initialize p = 0.
        alpha = 1 # Initial value for alpha.

        rho = np.zeros(self.max_iters).astype(dtype)
        rho[-1] = 1 # Silly trick so that rho[k-1] for k = 0 is defined.

        err = np.zeros(self.max_iters).astype(np.float64) # Error.

        temp, b_norm = self.rho_step(0, p, r, v, x) # Calculate norm of b, ||b||.
    
        if comm.Get_rank()==0:
            print 'b_norm check: ', b_norm # Make sure this isn't something bad like 0.
        t1 = time.time()
        
        # iteration for the solver
        for k in range(self.max_iters):
            if verbose.v>=2:
                print 'rho  ', k,
            rho[k], err0 = self.rho_step(alpha, p, r, v, x) 
            err[k] = abs(err0) / abs(b_norm) # Relative error.

            # Check termination condition.
            if verbose.v>=2 and comm.Get_rank()==0:
                t2 = time.time()
                print "   k=",k,", time=",t2-t1,",  err=",err[k]
            elif verbose.v<2 and comm.Get_rank()==0 and k%verbose.solverbase is 0:
                t2 = time.time()
                print "   k=",k,", time=",t2-t1,",  err=",err[k]

            if err[k] < self.err_thresh: # We successfully converged!
                if comm.Get_rank()==0:
                    t2 = time.time()
                    print "---k=",k,", time=",t2-t1,",  err=",err[k]
                for i in range(3):
                    x[i].synchronize()
                return x, err[:k+1], True

            if verbose.v>=2:
                print 'alpha', k,  
            alpha = self.alpha_step(rho[k], rho[k-1], p, r, v)

        # Return the answer, and the progress we made.
        if comm.Get_rank()==0:
            t2 = time.time()
            print "---k=",k,"  err=",err[k]
        for i in range(3):
            x[i].synchronize()
        return x, err, False

    def Multisolve(self, b, x=None):
        bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in b]
        # first solve
        ctrl=0
        x, err, success = self.bicg(bg,x)

        if success:
            return x, err, success
        # ----------------- fails with zero --------------------------------#
        if verbose.count<verbose.init and verbose.init_type == 'zero':
            if comm.rank==0:
                print "Now at solve",ctrl," not converged with zeros."
            ctrl += 1
            # first try with some random addition
            xc = [E.get() for E in x]
            if comm.rank==0:
                print "Now at solve",ctrl," try with small random addition."
                xc[:] = [xc[i]+dtype(0.2*np.linalg.norm(xc[i])*np.random.randn(*self.shape)) for i in range(3)]
                
            x = [Grid(dtype(f), x_overlap=1) for f in xc]
            bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in b]
            x, err, success = self.bicg(bg,x)

            if success:
                return x, err, success

            # second try with larger tol
            ctrl += 1
            if comm.rank == 0:
                print "Now at solve",ctrl," try with 10* tol."
            bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in b]
            self.err_thresh *= 10.0
            x, err, success = self.bicg(bg,x)
            self.err_thresh /= 10.0
            
            if success:
                return x, err, success            

            # third try with full random
            ctrl += 1
            xc = [E.get() for E in x]
            if comm.rank==0:
                print "Now at solve",ctrl," try with full random."
                xc[:] = [dtype(np.linalg.norm(xc[i])*np.random.randn(*self.shape)) for i in range(3)]
            else:
                xc=[None]*3
                
            x = [Grid(dtype(f), x_overlap=1) for f in xc]
            bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in b]
            self.err_thresh *= 10.0
            x, err, success = self.bicg(bg,x)
            self.err_thresh /= 10.0
            
            return x, err, success                        
        # ----------------- fails with rand --------------------------------#
        if verbose.count<verbose.init and verbose.init_type == 'rand':
            if comm.rank==0:
                print "Now at solve",ctrl," not converged with random."
            # first try with zeros
            ctrl += 1
            if comm.rank==0:
                print "Now at solve",ctrl," try with zero."
            x= self.zeros()
            bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in b]
            x, err, success = self.bicg(bg,x)

            if success:
                return x, err, success                        

            ctrl += 1
            # second try with larger tol
            if comm.rank == 0:
                print "Now at solve",ctrl," try with 10* tol."
            bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in b]
            self.err_thresh *= 10.0
            x, err, success = self.bicg(bg,x)
            self.err_thresh /= 10.0

            return x, err, success                                    
        # ----------------- fails with previous --------------------------------#
        if verbose.count>=verbose.init:
            if comm.rank==0:
                print "Now at solve",ctrl," not converged with previous solution."
            ctrl += 1
            # first try with some random addition
            xc = [E.get() for E in x]
            if comm.rank==0:
                print "Now at solve",ctrl," try with small random addition."
                xc[:] = [xc[i]+dtype(0.2*np.linalg.norm(xc[i])*np.random.randn(*self.shape)) for i in range(3)]
            x = [Grid(dtype(f), x_overlap=1) for f in xc]
            bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in b]
            x, err, success = self.bicg(bg,x)

            if success:
                return x, err, success

            # second try with larger tol                
            ctrl += 1
            if comm.rank == 0:
                print "Now at solve",ctrl," try with 10* tol."
            bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in b]
            self.err_thresh *= 10.0
            x, err, success = self.bicg(bg,x)
            self.err_thresh /= 10.0

            if success:
                return x, err, success
                
            # Third try with 0
            ctrl += 1
            if comm.rank == 0:
                print "Now at solve",ctrl," try with zero."
            x= self.zeros()
            bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in b]
            self.err_thresh *= 10.0
            x, err, success = self.bicg(bg,x)
            self.err_thresh /= 10.0

            return x, err, success
