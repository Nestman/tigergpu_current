import numpy as np
from scipy.sparse import csr_matrix as csr
from math import exp
from gce import verbose

class Filter:
    def __init__(self,Mx=10,My=10,bproj=0.0,R=0,alpha=1.0):
        self.Mx = Mx
        self.My = My
        self.bproj = bproj
        self.R = R
        self.alpha = alpha

        # density filter (a matrix) remains the same, so only generated once
        self.Df=self.f_density()
        
    # density filter for smoothing
    def f_density(self):
        dim = self.Mx*self.My
        n0 = 2*self.R+1
        indptr = [0]
        indices = []
        data = []

        ctrlt = 0
        for i in range(dim):
            xi = int(i/self.My)
            yi = i%self.My
            norm = 0.0
            ctrl = 0
            dtmp = []
            
            for j in range(n0**2):
                xj = int(j/n0)+xi-self.R
                yj = j%n0+yi-self.R

                if xj<0 or xj>=self.Mx or yj<0 or yj>=self.My:
                    continue

                ctrl += 1
                indices.append(xj*self.My+yj)
                weight = exp(-1.0*((xi-xj)**2+(yi-yj)**2)/self.alpha**2)
                norm += weight
                dtmp.append(weight)

            for j in range(ctrl):
                dtmp[j] /= norm

            data += dtmp
            ctrlt += ctrl
            indptr.append(ctrlt)

        D = csr((data, indices, indptr), shape=(dim, dim))
        return D

    # projection filter for binarization
    def f_proj(self,dof):
        eta = 0.5
        bproj = self.bproj

        dofnew = np.copy(dof)
        dofgrad = np.copy(dof)

        for i in range(dof.size):
            if dof[i]<=eta:
                dofnew[i] = eta * ( exp(-bproj*(1-dof[i]/eta)) - (1-dof[i]/eta)*exp(-bproj) )
                dofgrad[i] = eta * ( (bproj/eta)*exp(-bproj*(1-dof[i]/eta)) + exp(-bproj)/eta )
            else:
                dofnew[i] = (1-eta) * ( 1 - exp(-bproj*(dof[i]-eta)/(1-eta)) + (dof[i] - eta)/(1-eta) * exp(-bproj)) + eta
                dofgrad[i] = (1-eta) * ( bproj/(1-eta) * exp(-bproj*(dof[i]-eta)/(1-eta )) + exp(-bproj)/(1-eta) )
                
        return dofnew, dofgrad
        
    def new_object(self,old_object):
        def n_object(dof,grad):
            # export the original dof
            if verbose.export_in_object is 1 and verbose.count%verbose.outputbase is 0:
                np.savetxt(verbose.filename+'dofinit'+str(verbose.count)+'.txt', dof)
                
            #first apply density filter
            dofnew=self.Df.dot(dof)
            #second projection filter
            dofnew,dofgrad=self.f_proj(dofnew)

            #compute with old objective
            val = old_object(dofnew,grad)

            #compute gradient
            if (grad.size>0) and verbose.outputfield==0:
                #first projection filter
                grad[:] *= dofgrad[:]
                #second density filter
                grad[:] = self.Df.transpose().dot(grad)
            return val
        return n_object
