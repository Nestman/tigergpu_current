import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time
from math import pi

from tool import memory_report
from tool import write_results
from tool import initialize
from gce.grid import Grid
from gce import verbose

from zrad import Poynting_z2_slab
from filter import Filter #does this work syntactically?
dtype=np.complex128

#this file defines the nl constraints and objective function for the Pyntg_z minimax problem reformulated to be differentiable

#here compared to the original formulation, dof[0] is the dummy variable t, and dof[1:] is the topology optimization dofs. grad is modified in similar fashion. 

def Poynting_z2_slab_nlconstraint(shape,j,x1,x2,b,epsBkg,epsdiff,solver,A,hx,hy,hz,omega,Npmlz,Jdir,upper, Mx,My,bproj,R,alpha):
    def Pyntg_z2_nlc(dof,grad):

        Pyntg_z2_old = Poynting_z2_slab(shape,j,x1,x2,b,epsBkg,epsdiff,solver,A,hx,hy,hz,omega,Npmlz,upper)
        FT = Filter(Mx,My,bproj,R,alpha)
        Pyntg_z2 = FT.new_object(Pyntg_z2_old)

        #the nl constraint is t-Pyntg_z2_slab <= 0
#        if comm.rank==0:
        epsgrad = np.zeros(Mx*My)
        val = Pyntg_z2(dof[1:],epsgrad)
        
        if comm.rank==0:
            print "At freq = ",np.real(omega)/2/pi,", count ",verbose.count," and dipole orientation ",Jdir,", Poynting_z2 = ",val #output Pyntg_z2 for this particular orientation

        val = dof[0]-val #t-Pyntg_z2_slab is constraint value
#        else:
#            val = 0.0
#            epsgrad=[None]
#        val = comm.bcast(val)
        
        if grad.size>0:
            if comm.rank==0:
                grad[0] = 1.0
                grad[1:] = -epsgrad[:]
            
            grad[:] = comm.bcast(grad)
        
        memory_report(str(verbose.count)+'-')
        return val
    return Pyntg_z2_nlc

def Poynting_z2_dummy(dof,grad):
    if grad.size>0:
        if comm.rank==0:
            grad[:] = 0.0
            grad[0] = 1.0
            print "At count ",verbose.count," the dummy variable t = ",dof[0]
            
        grad[:] = comm.bcast(grad)
    memory_report(str(verbose.count)+'-')
    return dof[0]
